package model

type Title struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}
