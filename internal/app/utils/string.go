package utils

func Unique(existing map[string]struct{}, additional []string) []string {
	unique := []string{}
	for _, key := range additional {
		if _, ok := existing[key]; !ok {
			unique = append(unique, key)
		}
		existing[key] = struct{}{}
	}
	return unique
}
