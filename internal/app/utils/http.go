package utils

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
)

func Download(url string, dst string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("Wrong Status Code %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	out, err := Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		os.Remove(dst)
	}

	return err
}

func DownloadInto(url string, out *os.File) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("Wrong Status Code %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	_, err = io.Copy(out, resp.Body)
	return err
}
func DownloadRange(url string, dst string, start int64, end int64) error {
	req, _ := http.NewRequest("GET", url, nil)
	rangeHeader := bytesRange(start, end)
	req.Header.Add("Range", rangeHeader)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("Wrong Status Code %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	out, err := Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		os.Remove(dst)
	}

	return err
}

func bytesRange(start int64, end int64) string {
	range_header := "bytes=" + strconv.FormatInt(start, 10) + "-" + strconv.FormatInt(end, 10)
	return range_header
}
