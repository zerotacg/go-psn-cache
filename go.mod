module gitlab.com/zerotacg/go-psn-cache

go 1.15

require (
	github.com/fatih/structs v1.1.0
	github.com/gorilla/mux v1.8.0
	github.com/inhies/go-bytesize v0.0.0-20200716184324-4fe85e9b81b2
	golang.org/x/net v0.0.0-20200904194848-62affa334b73
)
