package tmdb

import (
	"gitlab.com/zerotacg/go-psn-cache/internal/app/cache"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/utils"
)

const BaseUrl = "tmdb.np.dl.playstation.net/tmdb2"
const TileIdPattern = "CUSA?????_??_*"
const TitleIdLengh = 12

func FindTitleIds(cache *cache.Cache) []string {
	matches := []string{}
	set := make(map[string]struct{})

	matches = append(matches, utils.Unique(set, transformPathToId(findTitlePaths(cache)))...)

	return matches
}

func transformPathToId(paths []string) []string {
	ids := []string{}
	for _, path := range paths {
		ids = append(ids, path[:TitleIdLengh])
	}
	return ids
}

func findTitlePaths(cache *cache.Cache) []string {
	paths := cache.ReadDir(BaseUrl, TileIdPattern)
	return paths
}
