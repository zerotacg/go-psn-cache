package main

import (
	"fmt"
	"gitlab.com/zerotacg/go-psn-cache/internal/app"
	"os"
	"strconv"
)

var version = "SNAPSHOT"

func main() {
	a := app.App{}
	cachePath := getEnv("APP_CACHE_PATH", "data/cache")
	queueSizeConfig := getEnv("APP_QUEUE_SIZE", "3")
	hosts := []string{
		"apollo.dl.playstation.net",
		"duk01.ps4.update.playstation.net",
		"gs2-sec.ww.prod.dl.playstation.net",
		"gs2.ww.prod.dl.playstation.net",
		"psn-rsc.prod.dl.playstation.net",
		"static-resource.np.community.playstation.net",
		"tmdb.np.dl.playstation.net",
		"trophy01.np.community.playstation.net",
	}
	queueSize, err := strconv.Atoi(queueSizeConfig)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	a.Initialize(cachePath, hosts, version, 128*1024*1024, queueSize)
	a.Run(":8080")
}

func getEnv(key string, defaultValue string) string {
	env := os.Getenv(key)
	if env == "" {
		env = defaultValue
	}
	return env
}
