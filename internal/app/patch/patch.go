package patch

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/xml"
	"fmt"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/cache"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/gs2"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/utils"
	"io/ioutil"
	"os"
	"strings"
)

var key, _ = hex.DecodeString("AD62E37F905E06BC19593142281C112CEC0E7EC3E97EFDCAEFCDBAAFA6378D84")

type TitlePatch struct {
	XMLName xml.Name `xml:"titlepatch"`
	TitleId string   `xml:"titleid,attr"`
	Tag     Tag      `xml:"tag"`
}

type Tag struct {
	XMLName              xml.Name             `xml:"tag"`
	Name                 string               `xml:"name,attr"`
	Mandatory            bool                 `xml:"mandatory,attr"`
	Package              Package              `xml:"package"`
	LatestPlayGoManifest LatestPlayGoManifest `xml:"latest_playgo_manifest"`
}

type Package struct {
	XMLName       xml.Name        `xml:"package"`
	Version       string          `xml:"version,attr"`
	Size          int64           `xml:"size,attr"`
	Digest        string          `xml:"digest,attr"`
	ManifestUrl   gs2.ManifestUrl `xml:"manifest_url,attr"` // "http://gs2.ww.prod.dl.playstation.net/gs2/ppkgo/prod/CUSA15362_00/44/f_94496deda8eed85bae7cfe746c2d868d5d7bef099ee0ce122e38f79eb64e4714/f/EP1006-CUSA15362_00-0000000000000000-A0206-V0100.json"
	ContentId     string          `xml:"content_id,attr"`   // "EP1006-CUSA15362_00-0000000000000000"
	SystemVersion string          `xml:"system_ver,attr"`   // "123011072"
	Type          string          `xml:"type,attr"`         // "cumulative"
	Remaster      bool            `xml:"remaster,attr"`     // "false"
	PatchGo       bool            `xml:"patchgo,attr"`      // "true"
	DeltaInfoSet  DeltaInfoSet    `xml:"delta_info_set"`
	Paramsfo      Paramsfo        `xml:"paramsfo"`
}

type DeltaInfoSet struct {
	XMLName xml.Name `xml:"delta_info_set"`
	Url     string   `xml:"url,attr"` // "http://gs2.ww.prod.dl.playstation.net/gs2/ppkgo/prod/CUSA15362_00/44/f_94496deda8eed85bae7cfe746c2d868d5d7bef099ee0ce122e38f79eb64e4714/f/EP1006-CUSA15362_00-0000000000000000-A0206-V0100-DP.pkg"
}
type Paramsfo struct {
	XMLName xml.Name `xml:"paramsfo"`
	Title   string   `xml:"title"`
}
type LatestPlayGoManifest struct {
	XMLName xml.Name `xml:"latest_playgo_manifest"`
	Url     string   `xml:"url,attr"` // "http://gs2.ww.prod.dl.playstation.net/gs2/ppkgo/prod/CUSA15362_00/44/f_94496deda8eed85bae7cfe746c2d868d5d7bef099ee0ce122e38f79eb64e4714/f/playgo-manifest.xml"
}

func GetVersion(titleId string, cache *cache.Cache) (*TitlePatch, error) {
	cachePath := cache.CachePath
	url := PatchUrl(titleId)
	dst := fmt.Sprintf("%s/tmp/version_%s.xml", cachePath, titleId)
	fmt.Printf("GetVersion: %s\n", url)
	err := utils.Download(url, dst)
	if err != nil {
		return nil, err
	}

	xmlFile, err := os.Open(dst)
	if err != nil {
		return nil, err
	}

	defer xmlFile.Close()

	byteValue, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		return nil, err
	}

	var titlePatch TitlePatch
	err = xml.Unmarshal(byteValue, &titlePatch)
	if err != nil {
		return nil, err
	}

	os.Remove(dst)

	return &titlePatch, err
}

func PatchUrl(titleId string) string {
	h := hmac.New(sha256.New, key)
	h.Write([]byte("np_" + titleId))
	hash := strings.ToLower(hex.EncodeToString(h.Sum(nil)))

	return fmt.Sprintf("http://gs-sec.ww.np.dl.playstation.net/plo/np/%s/%s/%s-ver.xml", titleId, hash, titleId)
}
