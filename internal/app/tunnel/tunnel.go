package tunnel

import (
	"io"
	"net"
	"net/http"
)

type Tunnel struct{}

func NewTunnel() *Tunnel {
	return new(Tunnel)
}

func (t *Tunnel) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	hj, ok := w.(http.Hijacker)
	if !ok {
		http.Error(w, "webserver doesn't support hijacking", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	conn, bufrw, err := hj.Hijack()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	bufrw.Flush()
	copyConn(conn, r.RequestURI)
	return
}

func copyConn(src net.Conn, RequestURI string) {
	dst, err := net.Dial("tcp", RequestURI)
	if err != nil {
		panic("Dial Error:" + err.Error())
	}

	done := make(chan struct{})

	go func() {
		defer dst.Close()
		io.Copy(dst, src)
		done <- struct{}{}
	}()

	go func() {
		defer dst.Close()
		io.Copy(src, dst)
		done <- struct{}{}
	}()

	<-done
	<-done
}
