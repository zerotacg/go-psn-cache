package tmdb

import (
	"gitlab.com/zerotacg/go-psn-cache/internal/app/cache"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/gs2"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/patch"
	"net/http"
)

type Title struct {
	Cache *cache.Cache
}

func NewTitle(cache *cache.Cache) *Title {
	instance := new(Title)
	instance.Cache = cache

	return instance
}

func (t *Title) CacheTitle(titleId string) {
	t.cacheVersion(titleId)
	t.cacheApps(titleId)
	t.cacheAdditionalContent(titleId)
}

func (t *Title) cacheVersion(titleId string) {
	version, _ := patch.GetVersion(titleId[0:9], t.Cache)
	manifestUrl := version.Tag.Package.ManifestUrl
	t.cacheManifest(manifestUrl)
}

func (t *Title) cacheManifest(manifestUrl gs2.ManifestUrl) {
	manifest, _ := gs2.GetManifest(manifestUrl, t.Cache)
	for _, piece := range manifest.Pieces {
		hop, _ := http.NewRequest("GET", piece.Url, nil)
		go t.Cache.AddQueued(hop)
	}
}

func (t *Title) cacheApps(titleId string) {
	for _, manifestUrl := range gs2.FindApps(t.Cache, titleId) {
		t.cacheManifest(manifestUrl)
	}
}

func (t *Title) cacheAdditionalContent(titleId string) {
	for _, manifestUrl := range gs2.FindAdditionalContents(t.Cache, titleId) {
		t.cacheManifest(manifestUrl)
	}
}
