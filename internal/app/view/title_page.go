package view

import (
	"fmt"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/cache"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/dom"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/gs2"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/patch"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/tmdb"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/utils/bytes"
	"net/url"
	"os"
	"path"
)

func TitlePage(cache *cache.Cache, titleId string) string {
	info, _ := tmdb.GetInfo(titleId, cache)
	name := titleId
	if info != nil {
		name = info.Names[0].Name
	}

	var icon dom.ReactElement
	if info != nil {
		icon = img(dom.Attr{Src: "/cache/get?url=" + info.Icons[0].Icon})
	}

	return dom.Render(dom.Fragment(
		"<!DOCTYPE html>",
		page("Package",
			nav(dom.Attr{AriaLabel: "breadcrumb"},
				ol(dom.Attr{Class: "breadcrumb"},
					li(dom.Attr{Class: "breadcrumb-item"}, a(dom.Attr{Href: "../.."}, "home")),
					li(dom.Attr{Class: "breadcrumb-item"}, a(dom.Attr{Href: ".."}, "titles")),
					li(dom.Attr{Class: "breadcrumb-item active"}, "package"),
				),
			),
			div(dom.Attr{Style: "background-image: url(/cache/get?url=" + info.BackgroundImage + ");"},
				div(dom.Attr{Style: "background-color: rgba(255,255,255,0.7);"},
					h2(dom.Attr{}, name),
					h3(dom.Attr{}, a(dom.Attr{Href: "add"}, "add")),
					icon, br(dom.Attr{}),
					renderVersion(cache, titleId),
					renderManifestUrls(cache, "Apps", gs2.FindApps(cache, titleId)),
					renderManifestUrls(cache, "Add-Ons", gs2.FindAdditionalContents(cache, titleId)),
					renderManifestUrls(cache, "Patches", gs2.FindPatches(cache, titleId)),
				),
			),
		),
	))
}

func renderVersion(cache *cache.Cache, titleId string) dom.ReactElement {
	var renderedVersion dom.ReactElement
	version, _ := patch.GetVersion(titleId[0:9], cache)
	if version != nil {
		return renderManifestStatus(cache, version.Tag.Package.Version, version.Tag.Package.ManifestUrl)
	}
	return renderedVersion
}

func renderManifestUrls(cache *cache.Cache, title string, manifestUrls []gs2.ManifestUrl) string {
	var listEntries []interface{}
	for _, manifestUrl := range manifestUrls {
		listEntries = append(listEntries, renderManifestStatus(cache, path.Base(string(manifestUrl)), manifestUrl))
	}

	return dom.Render(dom.Fragment(
		h3(dom.Attr{}, title),
		ul(dom.Attr{}, listEntries...),
	))
}

func renderManifestStatus(cache *cache.Cache, name string, manifestUrl gs2.ManifestUrl) dom.ReactElement {
	manifest, err := gs2.GetManifest(manifestUrl, cache)
	parsedManifestUrl, _ := url.Parse(string(manifestUrl))
	var size int64 = 0
	if err == nil {
		for _, piece := range manifest.Pieces {
			stat, _ := Stat(cache, piece.Url)
			if stat != nil {
				size += stat.Size()
			}
		}
	}

	var listEntries []interface{}
	for i, piece := range manifest.Pieces {
		stat, _ := Stat(cache, piece.Url)
		var size int64
		if stat != nil {
			size = stat.Size()
		}
		listEntries = append(listEntries, li(
			dom.Attr{},
			renderCacheActions(piece.Url),
			fmt.Sprintf(" - Piece %02d", i),
			" - ", bytes.HumanReadable(size), " / ", bytes.HumanReadable(piece.FileSize),
		))
	}

	return details(dom.Attr{},
		summary(dom.Attr{},
			a(dom.Attr{Href: fmt.Sprintf("/storage/%s/%s", parsedManifestUrl.Host, parsedManifestUrl.Path)}, name),
			fmt.Sprintf(" %s / %s", bytes.HumanReadable(size), bytes.HumanReadable(manifest.OriginalFileSize)),
		),
		ul(dom.Attr{}, listEntries...),
	)
}

func renderCacheActions(url string) string {
	return fmt.Sprintf(`%s %s `, renderCacheAddLink(url), renderCacheRemoveLink(url))
}

func renderCacheAddLink(url string) string {
	return fmt.Sprintf(`<a href="/cache/add?url=%s">add</a>`, url)
}

func renderCacheRemoveLink(url string) string {
	return fmt.Sprintf(`<a href="/cache/remove?url=%s">remove</a>`, url)
}

func Stat(cache *cache.Cache, rawUrl string) (os.FileInfo, error) {
	pieceUrl, err := url.Parse(rawUrl)
	if err != nil {
		return nil, err
	}
	return cache.Stat(pieceUrl)
}
