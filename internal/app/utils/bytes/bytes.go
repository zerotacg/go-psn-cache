package bytes

import "github.com/inhies/go-bytesize"

func HumanReadable(s int64) string {
	return bytesize.New(float64(s)).String()
}
