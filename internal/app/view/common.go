package view

import "gitlab.com/zerotacg/go-psn-cache/internal/app/dom"

var html = dom.Html
var head = dom.Head
var meta = dom.Meta
var title = dom.Title
var body = dom.Body
var div = dom.Div
var span = dom.Span
var ul = dom.Ul
var ol = dom.Ol
var li = dom.Li
var pre = dom.Pre
var h1 = dom.H1
var h2 = dom.H2
var h3 = dom.H3
var h4 = dom.H4
var h5 = dom.H5
var h6 = dom.H6
var a = dom.A
var link = dom.Link
var img = dom.Img
var br = dom.Br
var details = dom.Details
var summary = dom.Summary
var nav = dom.Nav
var script = dom.Script

func page(pageTitle interface{}, pageBody ...interface{}) dom.ReactElement {
	return dom.Fragment(
		"<!DOCTYPE html>",
		html(
			dom.Attr{
				Lang: "en",
			},
			head(
				dom.Attr{},
				meta(dom.Attr{
					Charset: "utf-8",
				}),
				meta(dom.Attr{
					Name:    "viewport",
					Content: "width=device-width, initial-scale=1",
				}),
				link(dom.Attr{
					Href:        "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css",
					Rel:         "stylesheet",
					Integrity:   "sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3",
					Crossorigin: "anonymous",
				}),
				title(
					dom.Attr{},
					pageTitle,
				),
			),
			body(
				dom.Attr{},
				div(
					dom.Attr{Class: "container-fluid"},
					pageBody...,
				),
				script(
					dom.Attr{
						Src:         "https://code.jquery.com/jquery-3.2.1.slim.min.js",
						Integrity:   "sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN",
						Crossorigin: "anonymous",
					},
				),
				script(
					dom.Attr{
						Src:         "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js",
						Integrity:   "sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q",
						Crossorigin: "anonymous",
					},
				),
				script(
					dom.Attr{
						Src:         "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
						Integrity:   "sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl",
						Crossorigin: "anonymous",
					},
				),
			),
		),
	)
}

//
//<!-- Bootstrap CSS -->
//<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" >
