package dom

import "fmt"
import "github.com/fatih/structs"

func Render(element ReactElement) string {
	return element.String()
}

type ReactElement struct {
	tag           string
	attributes    map[string]interface{}
	children      []interface{}
	isSelfClosing bool
}

type ReactElementList []interface{}

func (r ReactElement) String() string {
	buffer := ""
	var isSelfClosing = r.isSelfClosing
	if r.tag != "" {
		attributes := ""
		for key, value := range r.attributes {
			attributes += fmt.Sprintf(" %s=\"%s\"", key, value)
		}
		if isSelfClosing {
			buffer += fmt.Sprintf("<%s%s />", r.tag, attributes)
		} else {
			buffer += fmt.Sprintf("<%s%s>", r.tag, attributes)
		}
	}
	for _, element := range r.children {
		if element != nil {
			buffer += fmt.Sprintf("%s", element)
		}
	}
	if r.tag != "" && !isSelfClosing {
		buffer += fmt.Sprintf("</%s>", r.tag)
	}

	return buffer
}

func (e ReactElementList) String() string {
	buffer := ""
	for _, element := range e {
		buffer += fmt.Sprintf("%s", element)
	}

	return buffer
}

type Attr struct {
	Href        string `structs:"href,omitempty"`
	Lang        string `structs:"lang,omitempty"`
	Charset     string `structs:"charset,omitempty"`
	Name        string `structs:"name,omitempty"`
	Content     string `structs:"content,omitempty"`
	Rel         string `structs:"rel,omitempty"`
	Integrity   string `structs:"integrity,omitempty"`
	Crossorigin string `structs:"crossorigin,omitempty"`
	Class       string `structs:"class,omitempty"`
	Style       string `structs:"style,omitempty"`
	Src         string `structs:"src,omitempty"`
	AriaLabel   string `structs:"aria-label,omitempty"`
}

func Fragment(children ...interface{}) ReactElement {
	return ReactElement{children: children}
}

func elementFactory(tag string) func(attributes Attr, children ...interface{}) ReactElement {
	return func(attributes Attr, children ...interface{}) ReactElement {
		return ReactElement{tag: tag, attributes: structs.Map(attributes), isSelfClosing: false, children: children}
	}
}

func tagFactory(tag string) func(attributes Attr) ReactElement {
	return func(attributes Attr) ReactElement {
		return ReactElement{tag: tag, attributes: structs.Map(attributes), isSelfClosing: true}
	}
}

var Html = elementFactory("html")
var Head = elementFactory("head")
var Meta = elementFactory("meta")
var Title = elementFactory("title")
var Body = elementFactory("body")
var Div = elementFactory("div")
var Span = elementFactory("span")
var Ul = elementFactory("ul")
var Ol = elementFactory("ol")
var Li = elementFactory("li")
var Pre = elementFactory("pre")
var H1 = elementFactory("h1")
var H2 = elementFactory("h2")
var H3 = elementFactory("h3")
var H4 = elementFactory("h4")
var H5 = elementFactory("h5")
var H6 = elementFactory("h6")
var A = elementFactory("a")
var Link = elementFactory("link")
var Img = elementFactory("img")
var Br = tagFactory("br")
var Details = elementFactory("details")
var Summary = elementFactory("summary")
var Nav = elementFactory("nav")
var Script = elementFactory("script")
