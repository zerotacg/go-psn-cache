package tmdb

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/cache"
	"io/ioutil"
	"strings"
)

var key, _ = hex.DecodeString("F5DE66D2680E255B2DF79E74F890EBF349262F618BCAE2A9ACCDEE5156CE8DF2CDF2D48C71173CDC2594465B87405D197CF1AED3B7E9671EEB56CA6753C2E6B0")

type Info struct {
	Revision        int    `json:"revision"`
	FormatVersion   int    `json:"formatVersion"`
	NpTitleId       string `json:"npTitleId"`
	Console         string `json:"console"`
	Names           []Name `json:"names"`
	Icons           []Icon `json:"icons"`
	ParentalLevel   int    `json:"parentalLevel"`
	Pronunciation   string `json:"pronunciation"`
	ContentId       string `json:"contentId"`
	BackgroundImage string `json:"backgroundImage"`
	Category        string `json:"category"`
	PsVr            int    `json:"psVr"`
	NeoEnable       int    `json:"neoEnable"`
}

type Name struct {
	Name string `json:"name"`
}

type Icon struct {
	Icon string `json:"icon"`
	Type string `json:"type"`
}

func GetInfo(titleId string, cache *cache.Cache) (*Info, error) {
	url := InfoUrl(titleId)
	fmt.Printf("GetInfo: %s\n", url)

	file, err := cache.Open(url)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	byteValue, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var data Info
	err = json.Unmarshal(byteValue, &data)
	if err != nil {
		return nil, err
	}

	return &data, err
}

func InfoUrl(titleId string) string {
	h := hmac.New(sha1.New, key)
	h.Write([]byte(titleId))
	hash := strings.ToUpper(hex.EncodeToString(h.Sum(nil)))

	return fmt.Sprintf("http://tmdb.np.dl.playstation.net/tmdb2/%s_%s/%s.json", titleId, hash, titleId)
}

// Title.cache()
//   cacheVersion
//     version = getVersion
//     packages = getPackages(version)
//     cache.add(packages)
//   packages = findPackages
//   cache.add(packages)
