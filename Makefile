SOURCES = $(wildcard *.go)

.PHONY: clean default
.DEFAULT_GOAL := default

clean:

default: all
all: dist/psn-cache.tar

dist/psn-cache.tar: psn-cache dist
	docker save --output="$@" $<

psn-cache: dist/psn-cache.stamp

dist/psn-cache.stamp: VER=$(shell git rev-parse --short HEAD)
dist/psn-cache.stamp: Dockerfile docker-compose.yml dist
	VERSION=$(VER) docker-compose build --force-rm
	touch $@

Dockerfile: $(SOURCES)

dist:
	mkdir --parents $@