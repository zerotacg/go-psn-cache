FROM golang:1.15 AS build

WORKDIR /src
COPY go.* ./
RUN go mod download
COPY . .
RUN go build -ldflags "-X main.version=$VERSION" -o /out/app internal/main.go

FROM ubuntu AS bin
COPY --from=build /out/app /

WORKDIR /

ENV APP_CACHE_PATH=/data

EXPOSE 8080

CMD ["/app"]