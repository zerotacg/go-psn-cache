package app

import (
	"container/ring"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/cache"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/firmware"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/gs2"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/model"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/patch"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/proxy"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/tmdb"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/tunnel"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/utils"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/view"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strings"
	"time"
)

type App struct {
	Router  *mux.Router
	Proxy   *proxy.Proxy
	Tunnel  *tunnel.Tunnel
	Cache   *cache.Cache
	Title   *tmdb.Title
	Version string
}

var logs = ring.New(100)

func (a *App) Initialize(cachePath string, hosts []string, version string, chunkSize int64, queueSize int) {
	storagePath := cachePath + "/storage"
	a.Proxy = proxy.NewProxy()
	a.Tunnel = tunnel.NewTunnel()
	a.Cache = cache.NewCache(storagePath, a.Proxy, chunkSize, queueSize)
	a.Router = mux.NewRouter()
	a.Title = tmdb.NewTitle(a.Cache)
	a.Version = version
	cacheHandler := logRequest("cache", a.Cache)
	err := os.MkdirAll(storagePath, os.ModePerm)
	if err != nil {
		log.Printf("could not create storage directory %s", storagePath)
	}

	a.Router.SkipClean(true)
	a.Router.
		PathPrefix("").
		Methods("CONNECT").
		Handler(logRequest("tunnel", a.Tunnel))
	cacheRouter := a.Router.Methods("GET", "HEAD").Subrouter()
	for _, host := range hosts {
		cacheRouter.Host(host).
			PathPrefix("").
			Handler(cacheHandler)
	}
	a.Router.
		MatcherFunc(SchemeMatch("http")).
		PathPrefix("").
		Handler(logRequest("proxy", a.Proxy))

	local := a.Router.PathPrefix("").Subrouter()
	local.HandleFunc("/", a.getIndex)
	local.HandleFunc("/status", a.getStatus)
	local.HandleFunc("/logs/", a.getLogs)
	local.HandleFunc("/firmware/", a.getFirmware)
	local.HandleFunc("/titles/", a.getTitles)
	local.HandleFunc("/titles.json", a.getTitlesJson)
	local.HandleFunc("/titles/{titleId}_00/version.json", a.getVersion)
	local.HandleFunc("/titles/{titleId}/", a.getTitle)
	local.HandleFunc("/titles/{titleId}.json", a.getPackagesJson)
	local.HandleFunc("/titles/{titleId}/manifest/{manifest}/", a.getManifest)
	local.HandleFunc("/titles/{titleId}/add", func(res http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		titleId := vars["titleId"]

		go a.Title.CacheTitle(titleId)
	})
	local.
		PathPrefix("/cache/get").Handler(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		uri := req.URL.Query().Get("url")
		var next http.Handler = cacheHandler
		hop, err := http.NewRequest(req.Method, uri, req.Body)
		if err != nil {
			fmt.Printf("error: %s\n", err)
			next = http.NotFoundHandler()
		}
		next.ServeHTTP(res, hop)
	}))
	local.
		PathPrefix("/cache/add").Handler(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		uri := req.URL.Query().Get("url")
		hop, err := http.NewRequest(req.Method, uri, req.Body)
		if err != nil {
			fmt.Printf("error: %s\n", err)
			http.NotFoundHandler().ServeHTTP(res, hop)
		} else {
			go a.Cache.AddQueued(hop)
		}
	}))
	local.
		PathPrefix("/cache/remove").Handler(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		uri := req.URL.Query().Get("url")
		hop, err := http.NewRequest(req.Method, uri, req.Body)
		if err != nil {
			fmt.Printf("error: %s\n", err)
			http.NotFoundHandler().ServeHTTP(res, hop)
		} else {
			go a.Cache.Remove(hop)
		}
	}))
	local.
		PathPrefix("/").
		Handler(logRequest("storage", http.FileServer(http.Dir(cachePath))))
	local.PathPrefix("").Handler(http.NotFoundHandler())
}

func SchemeMatch(scheme string) func(request *http.Request, match *mux.RouteMatch) bool {
	return func(request *http.Request, match *mux.RouteMatch) bool {
		return request.URL.Scheme == scheme
	}
}

func logRequest(prefix string, next http.Handler) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		logs = logs.Prev()
		entry := fmt.Sprintf("[%s] [%s] Method: %s URI: %s", time.Now().Format("2006-01-02T15:04:05.000000000Z07:00"), prefix, req.Method, req.RequestURI)
		logs.Value = entry
		fmt.Println(entry)

		next.ServeHTTP(res, req)
	}
}

func (a *App) getIndex(writer http.ResponseWriter, request *http.Request) {
	body := view.IndexPage()
	writer.WriteHeader(http.StatusOK)
	_, _ = io.WriteString(writer, body)
}

type Status struct {
	Version string `json:"version"`
	Status  string `json:"status"`
}

func (a *App) getStatus(writer http.ResponseWriter, request *http.Request) {
	respondWithJSON(writer, http.StatusOK, a.appStatus())
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func (a *App) appStatus() Status {
	return Status{
		a.Version,
		"ok",
	}
}

func (a *App) getLogs(writer http.ResponseWriter, request *http.Request) {
	body := view.LogsPage(logEntries(logs))
	writer.WriteHeader(http.StatusOK)
	_, _ = io.WriteString(writer, body)
}

func logEntries(log *ring.Ring) []string {
	entries := []string{}

	log.Do(func(entry interface{}) {
		if entry != nil {
			entries = append(entries, entry.(string))
		}
	})

	return entries
}

func (a *App) getTitles(writer http.ResponseWriter, request *http.Request) {
	titles := a.findTitles()
	sort.Sort(NameSorter(titles))

	body := view.TitlesPage(titles)

	writer.WriteHeader(http.StatusOK)
	_, _ = io.WriteString(writer, body)
}

func (a *App) getTitle(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	titleId := vars["titleId"]

	body := view.TitlePage(a.Cache, titleId)

	writer.WriteHeader(http.StatusOK)
	_, _ = io.WriteString(writer, body)
}

type NameSorter []model.Title

func (a NameSorter) Len() int      { return len(a) }
func (a NameSorter) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a NameSorter) Less(i, j int) bool {
	return strings.ToLower(a[i].Name) < strings.ToLower(a[j].Name)
}

func (a *App) findTitles() []model.Title {
	ids := []string{}
	set := make(map[string]struct{})
	ids = append(ids, utils.Unique(set, tmdb.FindTitleIds(a.Cache))...)
	ids = append(ids, utils.Unique(set, gs2.FindTitleIds(a.Cache))...)

	titles := make([]model.Title, len(ids))
	for i, id := range ids {
		titles[i].Id = id
		titles[i].Name = id
		info, _ := tmdb.GetInfo(id, a.Cache)
		if info != nil {
			titles[i].Name = info.Names[0].Name
		}
	}
	return titles
}

func (a *App) getTitlesJson(writer http.ResponseWriter, request *http.Request) {
	respondWithJSON(writer, http.StatusOK, a.findTitles())
}

type PackageStatus struct {
	Packages []string  `json:"packages"`
	Info     tmdb.Info `json:"info"`
}

func (a *App) Stat(rawUrl string) (os.FileInfo, error) {
	pieceUrl, err := url.Parse(rawUrl)
	if err != nil {
		return nil, err
	}
	return a.Cache.Stat(pieceUrl)
}

func (a *App) findPackages(titleId string) []string {
	return gs2.ToString(gs2.FindPackages(a.Cache, titleId))
}

func (a *App) getPackagesJson(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	titleId := vars["titleId"]

	matches := a.findPackages(titleId)
	info, err := tmdb.GetInfo(titleId, a.Cache)
	if err == nil {
		body := PackageStatus{
			matches,
			*info,
		}
		respondWithJSON(writer, http.StatusOK, body)
	} else {
		writer.WriteHeader(http.StatusNotFound)
	}
}

func (a *App) getManifest(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	titleId := vars["titleId"]
	manifest := vars["manifest"]
	body := `
           <!DOCTYPE html>
           <html>
           <head>
             <title>Manifest</title>
           </head>
           <body>`
	body += `<h2>` + titleId + `</h2>
				<pre>` + fmt.Sprint(manifest) + `<pre>`
	body += `
			</body>
			</html>
`
	writer.WriteHeader(http.StatusOK)
	_, _ = io.WriteString(writer, body)
}

type Patch struct {
	Version  string `json:"version"`
	Size     int64  `json:"size"`
	Manifest string `json:"manifest"`
	Delta    string `json:"delta"`
}

func (a *App) getVersion(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	titleId := vars["titleId"]
	version, err := patch.GetVersion(titleId, a.Cache)
	if err == nil {
		body := Patch{
			version.Tag.Package.Version,
			version.Tag.Package.Size,
			string(version.Tag.Package.ManifestUrl),
			version.Tag.Package.DeltaInfoSet.Url,
		}
		respondWithJSON(writer, http.StatusOK, body)
	} else {
		fmt.Printf("error: %s\n", err)
		writer.WriteHeader(http.StatusNotFound)
	}
}

func (a *App) getFirmware(writer http.ResponseWriter, request *http.Request) {
	data, err := firmware.GetVersion(a.Cache)
	if err == nil {
		body := view.FirmwarePage(a.Cache, data)

		writer.WriteHeader(http.StatusOK)
		_, _ = io.WriteString(writer, body)
	} else {
		fmt.Printf("error: %s\n", err)
		writer.WriteHeader(http.StatusNotFound)
	}
}

func (a *App) Run(addr string) {
	a.Cache.ClearTemp()
	log.Printf("listening %v\n", addr)
	log.Fatal(http.ListenAndServe(addr, a.Router))
}
