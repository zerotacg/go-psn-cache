package view

import (
	"gitlab.com/zerotacg/go-psn-cache/internal/app/cache"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/dom"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/firmware"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/utils/bytes"
	"net/url"
)

func FirmwarePage(cache *cache.Cache, data *firmware.Update) string {
	return dom.Render(
		page(
			"Firmware",
			nav(dom.Attr{AriaLabel: "breadcrumb"},
				ol(dom.Attr{Class: "breadcrumb"},
					li(dom.Attr{Class: "breadcrumb-item"}, a(dom.Attr{Href: ".."}, "home")),
					li(dom.Attr{Class: "breadcrumb-item active"}, "firmware"),
				),
			),
			h3(dom.Attr{}, "version: ", data.System.Label),
			image(cache, data.System.UpdateData.Image),
			image(cache, data.Recovery.Image),
		),
	)
}

func image(cache *cache.Cache, image firmware.Image) dom.ReactElement {
	loadedSize, _ := LoadedSize(cache, image.Url)

	return ul(
		dom.Attr{},
		li(dom.Attr{}, "size: ", bytes.HumanReadable(loadedSize), " / ", bytes.HumanReadable(image.Size)),
		li(dom.Attr{}, a(dom.Attr{Href: "/cache/add?url=" + image.Url}, "add")),
		li(dom.Attr{}, "url: ", image.Url),
	)
}

func LoadedSize(cache *cache.Cache, rawUrl string) (int64, error) {
	var size int64
	pieceUrl, err := url.Parse(rawUrl)
	if err != nil {
		return size, err
	}
	info, err := cache.Stat(pieceUrl)

	if err != nil {
		return size, err
	}

	return info.Size(), nil
}
