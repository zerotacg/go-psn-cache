package dom

import (
	"testing"
)

func TestProxy(t *testing.T) {
	t.Run("should render an empty tag", func(t *testing.T) {
		AssertThat(t, Render(Html(Attr{}, ""))).IsEqualTo("<html></html>")
	})

	t.Run("should render tag with text children", func(t *testing.T) {
		AssertThat(t, Render(Div(Attr{}, "text child"))).IsEqualTo("<div>text child</div>")
	})

	t.Run("should render Fragment without tag", func(t *testing.T) {
		AssertThat(t, Render(Fragment("some-", "child"))).IsEqualTo("some-child")
	})

	t.Run("should render span", func(t *testing.T) {
		AssertThat(t, Render(Span(Attr{}, "some-", "child"))).IsEqualTo("<span>some-child</span>")
	})

	t.Run("should render tag with attributes", func(t *testing.T) {
		AssertThat(t, Render(A(Attr{Href: "path/to/page.html"}, "some-", "child"))).IsEqualTo("<a href=\"path/to/page.html\">some-child</a>")
	})

	t.Run("should not render nil children", func(t *testing.T) {
		AssertThat(t, Render(Span(Attr{}, "some-", "child", nil, ",after nil"))).IsEqualTo("<span>some-child,after nil</span>")
	})

	t.Run("should render self closing tags", func(t *testing.T) {
		AssertThat(t, Render(Br(Attr{}))).IsEqualTo("<br />")
	})

	t.Run("should not render ul as self closing", func(t *testing.T) {
		AssertThat(t, Render(Ul(Attr{}))).IsEqualTo("<ul></ul>")
	})
}

type Assert struct {
	t     *testing.T
	value interface{}
}

func AssertThat(t *testing.T, actual interface{}) Assert {
	return Assert{t: t, value: actual}
}

func (a Assert) IsEqualTo(expected string) {
	actual := a.value
	if actual != expected {
		a.t.Errorf("expected %s but was: %s", expected, actual)
	}

}
