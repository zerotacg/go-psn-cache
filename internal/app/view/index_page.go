package view

import "gitlab.com/zerotacg/go-psn-cache/internal/app/dom"

func IndexPage() string {
	return dom.Render(
		page(
			"Cache",
			nav(dom.Attr{AriaLabel: "breadcrumb"},
				ol(dom.Attr{Class: "breadcrumb"},
					li(dom.Attr{Class: "breadcrumb-item active"}, "home"),
				),
			),
			ul(
				dom.Attr{},
				li(dom.Attr{}, a(dom.Attr{Href: "titles/"}, "titles")),
				li(dom.Attr{}, a(dom.Attr{Href: "firmware/"}, "firmware")),
				li(dom.Attr{}, a(dom.Attr{Href: "logs/"}, "logs")),
				li(dom.Attr{}, a(dom.Attr{Href: "storage"}, "storage")),
			),
		),
	)
}
