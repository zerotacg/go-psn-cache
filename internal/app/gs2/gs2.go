package gs2

import (
	"encoding/json"
	"fmt"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/cache"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/utils"
	"io/ioutil"
)

type Manifest struct {
	OriginalFileSize   int64   `json:"originalFileSize"`
	PackageDigest      string  `json:"packageDigest"`
	NumberOfSplitFiles int     `json:"numberOfSplitFiles"`
	Pieces             []Piece `json:"pieces"`
}

type Piece struct {
	Url        string `json:"url"` // "http://gs2.ww.prod.dl.playstation.net/gs2/appkgo/prod/CUSA00004_00/3/f_5b838ce58a016e810c1feef7c2414ca93a1fcd6334166d3ecbbf205d059d970e/f/EP9000-CUSA00004_00-SECONDSONSHIP000_0.pkg"
	FileOffset int64  `json:"fileOffset"`
	FileSize   int64  `json:"fileSize"`
	HashValue  string `json:"hashValue"`
}

type ManifestUrl string

const BaseUrl = "gs2.ww.prod.dl.playstation.net/gs2"
const TileIdPattern = "CUSA?????_??"

func GetManifest(url ManifestUrl, cache *cache.Cache) (*Manifest, error) {
	fmt.Printf("GetManifest: %s\n", url)

	file, err := cache.Open(string(url))
	if err != nil {
		return nil, err
	}

	defer file.Close()

	byteValue, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var data Manifest
	err = json.Unmarshal(byteValue, &data)
	if err != nil {
		return nil, err
	}

	return &data, err
}

func FindPackages(cache *cache.Cache, titleId string) []ManifestUrl {
	matches := []ManifestUrl{}

	matches = append(matches, FindAdditionalContents(cache, titleId)...)
	matches = append(matches, FindApps(cache, titleId)...)
	matches = append(matches, FindPatches(cache, titleId)...)

	return matches
}

func FindAdditionalContents(cache *cache.Cache, titleId string) []ManifestUrl {
	return findManifestUrls(cache, AdditionalContentBaseUrl(), titleId)
}

func FindApps(cache *cache.Cache, titleId string) []ManifestUrl {
	return findManifestUrls(cache, AppBaseUrl(), titleId)
}

func findManifestUrls(cache *cache.Cache, baseUrl string, titleId string) []ManifestUrl {
	root := fmt.Sprintf("%s/%s", baseUrl, titleId)
	return toManifestUrls(cache.FindFiles(root, "*/f_*/f/*.json"))
}

func toManifestUrls(s []string) []ManifestUrl {
	u := make([]ManifestUrl, len(s))
	for i, v := range s {
		u[i] = ManifestUrl(v)
	}
	return u
}

func FindPatches(cache *cache.Cache, titleId string) []ManifestUrl {
	return findManifestUrls(cache, PatchBaseUrl(), titleId)
}

func ToString(s []ManifestUrl) []string {
	u := make([]string, len(s))
	for i, v := range s {
		u[i] = string(v)
	}
	return u
}

func FindTitleIds(cache *cache.Cache) []string {
	matches := []string{}
	set := make(map[string]struct{})

	matches = append(matches, utils.Unique(set, cache.ReadDir(AppBaseUrl(), TileIdPattern))...)
	matches = append(matches, utils.Unique(set, cache.ReadDir(AdditionalContentBaseUrl(), TileIdPattern))...)
	matches = append(matches, utils.Unique(set, cache.ReadDir(PatchBaseUrl(), TileIdPattern))...)

	return matches
}

func AppBaseUrl() string {
	return fmt.Sprintf("%s/appkgo/prod", BaseUrl)
}

func PatchBaseUrl() string {
	return fmt.Sprintf("%s/ppkgo/prod", BaseUrl)
}

func AdditionalContentBaseUrl() string {
	return fmt.Sprintf("%s/acpkgo/prod", BaseUrl)
}
