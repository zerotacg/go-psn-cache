package view

import (
	"fmt"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/dom"
)

func LogsPage(logEntries []string) string {
	var lines []interface{}

	for _, entry := range logEntries {
		lines = append(lines, fmt.Sprintln(entry))
	}

	return dom.Render(
		page(
			"Logs",
			nav(dom.Attr{AriaLabel: "breadcrumb"},
				ol(dom.Attr{Class: "breadcrumb"},
					li(dom.Attr{Class: "breadcrumb-item"}, a(dom.Attr{Href: ".."}, "home")),
					li(dom.Attr{Class: "breadcrumb-item active"}, "logs"),
				),
			),
			pre(dom.Attr{}, lines...),
		),
	)
}
