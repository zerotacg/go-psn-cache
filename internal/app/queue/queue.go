package queue

import (
	"fmt"
)

type Queue struct {
	size int
	jobs chan Callback
}

type Callback func()

func New(workerCount int) *Queue {
	queue := new(Queue)
	queue.size = 0
	queue.jobs = make(chan Callback)
	for w := 1; w <= workerCount; w++ {
		go worker(w, queue.jobs)
	}

	return queue
}

func worker(id int, jobs <-chan Callback) {
	for j := range jobs {
		fmt.Println("worker", id, "started job")
		j()
		fmt.Println("worker", id, "finished job")
	}
}

func (q *Queue) Push(callback Callback) {
	q.jobs <- callback
}
