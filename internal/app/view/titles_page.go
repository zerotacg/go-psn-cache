package view

import (
	"gitlab.com/zerotacg/go-psn-cache/internal/app/dom"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/model"
)

func TitlesPage(titles []model.Title) string {
	var listEntries []interface{}
	for _, title := range titles {
		listEntries = append(listEntries, li(
			dom.Attr{},
			a(dom.Attr{Href: title.Id + "/"}, title.Name),
		))
	}

	return dom.Render(
		page(
			"Packages",
			nav(dom.Attr{AriaLabel: "breadcrumb"},
				ol(dom.Attr{Class: "breadcrumb"},
					li(dom.Attr{Class: "breadcrumb-item"}, a(dom.Attr{Href: ".."}, "home")),
					li(dom.Attr{Class: "breadcrumb-item active"}, "titles"),
				),
			),
			ul(
				dom.Attr{},
				listEntries...,
			),
		),
	)
}
