package utils

import (
	"os"
	"path"
)

func Create(name string) (*os.File, error) {
	err := os.MkdirAll(path.Dir(name), os.ModePerm)

	if err != nil {
		return nil, err
	}

	return os.Create(name)
}

func Rename(oldpath, newpath string) error {
	err := os.MkdirAll(path.Dir(newpath), os.ModePerm)

	if err != nil {
		return err
	}

	return os.Rename(oldpath, newpath)
}

func Remove(name string) error {
	return os.Remove(name)
}
