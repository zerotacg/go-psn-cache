package cache

import (
	"crypto/sha1"
	"fmt"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/queue"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/utils"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
)

type Cache struct {
	CachePath string
	TempPath  string
	Next      http.Handler
	ChunkSize int64
	Queue     *queue.Queue
}

func NewCache(cachePath string, next http.Handler, chunkSize int64, queueSize int) *Cache {
	cache := new(Cache)
	cache.CachePath = cachePath
	cache.TempPath = fmt.Sprintf("%s/tmp", cachePath)
	cache.ChunkSize = chunkSize
	cache.Next = next
	cache.Queue = queue.New(queueSize)

	return cache
}

func (c *Cache) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	if c.isCached(req) {
		res.Header().Set("X-Cache", "HIT")
		fileServer := c.cacheFileServer(req)
		fileServer.ServeHTTP(res, req)
	} else {
		res.Header().Set("X-Cache", "MISS")
		defer c.Add(req)
		c.Next.ServeHTTP(res, req)
	}
}

func (c *Cache) Open(url string) (*os.File, error) {
	req, _ := http.NewRequest("GET", url, nil)
	cacheEntry := c.cacheEntryPath(req.URL)
	isCached := exists(cacheEntry)
	status := "hit"
	if !isCached {
		status = "miss"
		err := c.Download(req)
		if err != nil {
			return nil, err
		}
	}
	fmt.Printf("cache %s %s\n", status, req.URL)
	return os.Open(cacheEntry)
}

func (c *Cache) Download(req *http.Request) error {
	url := req.URL.String()
	filepath := c.cacheEntryPath(req.URL)
	tempFile := c.cacheTempPath(req.URL)
	fmt.Printf("[%s] uri: %s dst: %s\n", "cache", url, filepath)
	if exists(filepath) {
		return nil
	}
	if exists(tempFile) {
		return fmt.Errorf("already downloading to %s", tempFile)
	}

	err := utils.Download(url, tempFile)
	if err != nil {
		return err
	}

	return utils.Rename(tempFile, filepath)
}

func (c *Cache) Add(req *http.Request) error {
	url := req.URL.String()
	filepath := c.cacheEntryPath(req.URL)
	tempFile := c.cacheTempPath(req.URL)
	fmt.Printf("[%s] uri: %s dst: %s\n", "cache", url, filepath)
	if exists(tempFile) || exists(filepath) {
		return nil
	}
	err := utils.Download(url, tempFile)
	if err != nil {
		return err
	}

	return utils.Rename(tempFile, filepath)
}

func (c *Cache) AddQueued(req *http.Request) error {
	url := req.URL.String()
	filepath := c.cacheEntryPath(req.URL)
	tempFile := c.cacheTempPath(req.URL)
	fmt.Printf("[%s] uri: %s dst: %s\n", "cache", url, filepath)
	if exists(tempFile) || exists(filepath) {
		return nil
	}
	out, err := utils.Create(tempFile)
	if err != nil {
		fmt.Printf("[%s] error creating temp file: %s %s\n", "cache", tempFile, err)
		return err
	}
	c.Queue.Push(func() {
		fmt.Printf("[%s] downloading started uri: %s dst: %s\n", "cache", url, filepath)
		download(out, url, tempFile, filepath)
		fmt.Printf("[%s] downloading finished uri: %s dst: %s\n", "cache", url, filepath)
	})

	return nil
}

func download(out *os.File, url string, tempFile string, filepath string) {
	defer out.Close()

	fmt.Printf("[%s] start downloadung url: %s\n", "cache", url)
	err := utils.DownloadInto(url, out)
	if err != nil {
		fmt.Printf("[%s] error downloadung url: %s %s\n", "cache", url, err)
		utils.Remove(tempFile)
		return
	}

	fmt.Printf("[%s] start renaming file src: %s dst: %s\n", "cache", tempFile, filepath)
	err = utils.Rename(tempFile, filepath)
	if err != nil {
		fmt.Printf("[%s] error renaming file src: %s dst: %s %s\n", "cache", tempFile, filepath, err)
	} else {
		fmt.Printf("[%s] renaming file src: %s dst: %s\n", "cache", tempFile, filepath)
	}
}

func (c *Cache) Remove(req *http.Request) error {
	url := req.URL.String()
	filepath := c.cacheEntryPath(req.URL)
	fmt.Printf("[%s] uri: %s dst: %s\n", "cache", url, filepath)
	if !exists(filepath) {
		return nil
	}
	return utils.Remove(filepath)
}

func (c *Cache) Stat(url *url.URL) (os.FileInfo, error) {
	filepath := c.cacheEntryPath(url)
	tempFile := c.cacheTempPath(url)
	if exists(filepath) {
		return os.Stat(filepath)
	}
	return os.Stat(tempFile)
}

func info(url string) (int64, error) {
	resp, err := http.Head(url)
	if err != nil {
		return 0, err
	}
	status := resp.StatusCode
	if status != 200 {
		return 0, fmt.Errorf("Wrong Status Code %d", status)
	}

	acceptedRanges := resp.Header.Get("Accept-Ranges")
	if acceptedRanges != "bytes" {
		return 0, fmt.Errorf("Accepted ranges not supported %s", acceptedRanges)
	}

	return resp.ContentLength, nil
}

func cacheKey(url *url.URL) string {
	return fmt.Sprintf("%s/%s", url.Host, url.Path)
}

func cacheTempKey(url *url.URL) string {
	key := cacheKey(url)
	base := path.Base(key)
	h := sha1.New()
	h.Write([]byte(key))
	hash := h.Sum(nil)
	return fmt.Sprintf("%x_%s.part", hash, base)
}

func (c *Cache) cacheEntryPath(url *url.URL) string {
	return fmt.Sprintf("%s/%s", c.CachePath, cacheKey(url))
}

func (c *Cache) cacheTempPath(url *url.URL) string {
	return fmt.Sprintf("%s/%s", c.TempPath, cacheTempKey(url))
}

func (c *Cache) cacheHostDir(req *http.Request) http.Dir {
	return http.Dir(c.cacheHostPath(req.URL))
}

func (c *Cache) cacheHostPath(url *url.URL) string {
	return fmt.Sprintf("%s/%s", c.CachePath, url.Host)
}

func (c *Cache) cacheFileServer(req *http.Request) http.Handler {
	return http.FileServer(c.cacheHostDir(req))
}

func (c *Cache) isCached(req *http.Request) bool {
	cacheEntry := c.cacheEntryPath(req.URL)
	isCached := exists(cacheEntry)
	status := "miss"
	if isCached {
		status = "hit"
	}
	fmt.Printf("cache %s %s\n", status, req.URL)

	return isCached
}

func (c *Cache) Status(req *http.Request) string {
	cached := c.cacheEntryPath(req.URL)
	loading := c.cacheTempPath(req.URL)
	if exists(cached) {
		return "cached"
	} else if exists(loading) {
		return "loading"
	} else {
		return "none"
	}
}

func exists(cacheEntry string) bool {
	_, err := os.Stat(cacheEntry)

	return err == nil
}

func (c *Cache) Clear() {
	os.RemoveAll(c.CachePath)
}

func (c *Cache) ClearTemp() {
	os.RemoveAll(c.TempPath)
}

func (c *Cache) ReadDir(root string, pattern string) []string {
	path := fmt.Sprintf("%s/%s", c.CachePath, root)

	matches := []string{}
	infos, _ := ioutil.ReadDir(path)
	for _, info := range infos {
		if info.IsDir() {
			searchPath := info.Name()
			matched, _ := filepath.Match(pattern, searchPath)
			if matched {
				matches = append(matches, searchPath)
			}
		}
	}

	return matches
}

func (c *Cache) FindFiles(root string, pattern string) []string {
	fullPattern := fmt.Sprintf("%s/%s", root, pattern)
	cachePath := c.CachePath
	matches := []string{}

	_ = filepath.Walk(fmt.Sprintf("%s/%s", cachePath, root), func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		searchPath, err := filepath.Rel(cachePath, path)
		matched, err := filepath.Match(fullPattern, searchPath)
		if err != nil {
			return err
		} else if matched {
			matches = append(matches, searchPath)
		}
		return nil
	})

	return matches
}
