package app

import (
	"fmt"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/utils"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

var a App
var cachePath = "/tmp/data/cache"

func TestProxy(t *testing.T) {
	a.Initialize(cachePath, []string{}, "TESTING", 1024, 1)

	t.Run("simple api test", func(t *testing.T) {
		req, _ := http.NewRequest("GET", "/status", nil)
		response := executeRequest(req)

		assertResponseCode(t, response, http.StatusOK)
		assertResponseBody(t, response, "{\"version\":\"TESTING\",\"status\":\"ok\"}")
	})

	t.Run("return body of upstream server", func(t *testing.T) {
		expected := "Hello, client"
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprint(w, expected)
		}))
		defer ts.Close()
		req := createProxyRequest("GET", ts.URL+"/")

		response := executeRequest(req)

		assertResponseCode(t, response, http.StatusOK)
		assertResponseBody(t, response, expected)
	})

	t.Run("return some header of upstream server", func(t *testing.T) {
		expected := "application/json"
		header := "Content-Type"
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set(header, expected)
		}))
		defer ts.Close()

		req := createProxyRequest("GET", ts.URL+"/")
		response := executeRequest(req)

		assertResponseCode(t, response, http.StatusOK)
		assertResponseHeader(t, response, header, expected)
	})

	t.Run("return 502 if upstream server is invalid", func(t *testing.T) {
		req := createProxyRequest("GET", "http://nonexisting.domain:12345/")
		response := executeRequest(req)

		assertResponseCode(t, response, http.StatusBadGateway)
	})

	t.Run("cache the upstream response", func(t *testing.T) {
		expected := "Hello, client"
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprint(w, expected)
		}))
		uri := fmt.Sprintf("%s/other.json", ts.URL)
		req := createProxyRequest("GET", uri)
		a.Initialize(cachePath, []string{req.URL.Host}, "", 1024, 1)
		executeRequest(req)
		time.Sleep(time.Second)
		ts.Close()

		response := executeRequest(req)

		assertResponseCode(t, response, http.StatusOK)
		assertResponseBody(t, response, expected)
	})

	t.Run("return size of cached entry", func(t *testing.T) {
		remoteResponse := "1234567890"
		contentLength := len(remoteResponse)
		ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprint(w, remoteResponse)
		}))
		uri := fmt.Sprintf("%s/other.json", ts.URL)
		req := createProxyRequest("GET", uri)
		a.Initialize(cachePath, []string{req.URL.Host}, "", 1024, 1)
		executeRequest(req)
		time.Sleep(time.Second)
		ts.Close()

		response := executeProxyRequest("HEAD", uri)

		assertResponseCode(t, response, http.StatusOK)
		assertResponseHeader(t, response, "Content-Length", fmt.Sprint(contentLength))
	})

	t.Run("return found titles", func(t *testing.T) {
		remoteResponse := "{}"
		name := "gs2.ww.prod.dl.playstation.net/gs2/appkgo/prod/CUSA00004_00/3/f_5b838ce58a016e810c1feef7c2414ca93a1fcd6334166d3ecbbf205d059d970e/f/EP9000-CUSA00004_00-SECONDSONSHIP000.json"
		createFile(name, remoteResponse)
		titleInfo := "{\"names\":[{\"name\":\"test-title-name\"}],\"icons\":[{\"icon\":\"\",\"type\":\"512x512\"}]}"
		createFile("tmdb.np.dl.playstation.net/tmdb2/CUSA00004_00_2B602E7E5B76130985480643D23EC19AF3EA8ECA/CUSA00004_00.json", titleInfo)
		req := createProxyRequest("GET", "/titles.json")
		a.Initialize(cachePath, []string{}, "", 1024, 1)
		response := executeRequest(req)

		assertResponseCode(t, response, http.StatusOK)
		assertResponseBody(t, response, "[{\"id\":\"CUSA00004_00\",\"name\":\"test-title-name\"}]")
	})

	t.Run("return found packages for title", func(t *testing.T) {
		remoteResponse := "{}"
		name := "gs2.ww.prod.dl.playstation.net/gs2/appkgo/prod/CUSA00004_00/3/f_5b838ce58a016e810c1feef7c2414ca93a1fcd6334166d3ecbbf205d059d970e/f/EP9000-CUSA00004_00-SECONDSONSHIP000.json"
		createFile(name, remoteResponse)
		titleInfo := "{\"names\":[{\"name\":\"test-title-name\"}],\"icons\":[{\"icon\":\"\",\"type\":\"512x512\"}]}"
		createFile("tmdb.np.dl.playstation.net/tmdb2/CUSA00004_00_2B602E7E5B76130985480643D23EC19AF3EA8ECA/CUSA00004_00.json", titleInfo)
		req := createProxyRequest("GET", "/titles/CUSA00004_00.json")
		a.Initialize(cachePath, []string{}, "", 1024, 1)
		response := executeRequest(req)

		assertResponseCode(t, response, http.StatusOK)
		assertResponseBody(t, response, "{\"packages\":[\"gs2.ww.prod.dl.playstation.net/gs2/appkgo/prod/CUSA00004_00/3/f_5b838ce58a016e810c1feef7c2414ca93a1fcd6334166d3ecbbf205d059d970e/f/EP9000-CUSA00004_00-SECONDSONSHIP000.json\"],\"info\":{\"revision\":0,\"formatVersion\":0,\"npTitleId\":\"\",\"console\":\"\",\"names\":[{\"name\":\"test-title-name\"}],\"icons\":[{\"icon\":\"\",\"type\":\"512x512\"}],\"parentalLevel\":0,\"pronunciation\":\"\",\"contentId\":\"\",\"backgroundImage\":\"\",\"category\":\"\",\"psVr\":0,\"neoEnable\":0}}")
	})

	a.Cache.Clear()
}

func createFile(name string, content string) {
	out, _ := utils.Create(fmt.Sprintf("%s/storage/%s", cachePath, name))
	defer out.Close()
	fmt.Fprint(out, content)
}

func executeProxyRequest(method string, uri string) *httptest.ResponseRecorder {
	request := createProxyRequest(method, uri)
	response := executeRequest(request)

	return response
}

func createProxyRequest(method string, uri string) *http.Request {
	req, _ := http.NewRequest(method, uri, nil)
	fmt.Printf("request: %s\n", req.URL.RequestURI())
	return req
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

func assertResponseCode(t *testing.T, response *httptest.ResponseRecorder, expected int) {
	t.Helper()
	if actual := response.Code; actual != expected {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func assertResponseBody(t *testing.T, response *httptest.ResponseRecorder, expected string) {
	t.Helper()
	if body := response.Body.String(); body != expected {
		t.Errorf("Expected response body \n'%s'\nGot\n'%s'", expected, body)
	}
}
func assertResponseHeader(t *testing.T, response *httptest.ResponseRecorder, header string, expected string) {
	t.Helper()
	if actual := response.Header().Get(header); actual != expected {
		t.Errorf("Expected response header '%s' to be '%s'  but Got '%s'", header, expected, actual)
	}
}
