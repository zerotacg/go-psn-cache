package firmware

import (
	"encoding/xml"
	"fmt"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/cache"
	"gitlab.com/zerotacg/go-psn-cache/internal/app/utils"
	"io/ioutil"
	"os"
)

const UpdateUrl = "http://fuk01.ps4.update.playstation.net/update/ps4/list/uk/ps4-updatelist.xml"

type Update struct {
	XMLName  xml.Name    `xml:"update_data_list"`
	System   SystemPup   `xml:"region>system_pup"`
	Recovery RecoveryPup `xml:"region>recovery_pup"`
}

type Region struct {
	XMLName     xml.Name    `xml:"region"`
	Id          string      `xml:"id,attr"`
	SystemPup   SystemPup   `xml:"system_pup"`
	RecoveryPup RecoveryPup `xml:"recovery_pup"`
}

type SystemPup struct {
	XMLName    xml.Name `xml:"system_pup"`
	Label      string   `xml:"label,attr"`
	UpdateData UpdateData
}

type UpdateData struct {
	XMLName xml.Name `xml:"update_data"`
	Image   Image    `xml:"image"`
}

type Image struct {
	XMLName xml.Name `xml:"image"`
	Size    int64    `xml:"size,attr"`
	Url     string   `xml:",chardata"`
}

type RecoveryPup struct {
	XMLName xml.Name `xml:"recovery_pup"`
	Image   Image    `xml:"image"`
}

func GetVersion(cache *cache.Cache) (*Update, error) {
	cachePath := cache.CachePath
	url := UpdateUrl
	dst := fmt.Sprintf("%s/tmp/ps4-updatelist.xml", cachePath)
	fmt.Printf("GetFirmwareVersion: %s\n", url)
	err := utils.Download(url, dst)
	if err != nil {
		return nil, err
	}

	xmlFile, err := os.Open(dst)
	if err != nil {
		return nil, err
	}

	defer xmlFile.Close()

	byteValue, err := ioutil.ReadAll(xmlFile)
	if err != nil {
		return nil, err
	}

	var data Update
	err = xml.Unmarshal(byteValue, &data)
	if err != nil {
		return nil, err
	}

	os.Remove(dst)

	return &data, err
}
